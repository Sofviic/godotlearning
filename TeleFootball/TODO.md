**TODO/Specs:**
<style>
r { color: Red }
o { color: Orange }
g { color: Green }
gy { color: Gray }
</style>

<gy>unstarted</gy> - <r>incomplete</r> - <o>unchecked</o> - <g>checked</g>

0. <r>add the rest of specs</r>
1. <o>auto add new settings to settings file</o>
2. <o>add options menu</o>
	1. <o>reset tutorial</o>
	2. <o>add keybinds menu</o>
		1. <o>add keybinds changing</o>
		2. <gy>controller support</gy>
		3. <gy>add collapsable categories</gy>
	3. <o>fov + fov sprint factor</o>
	4. <o>reset settings</o>
	5. <gy>add cheat toggle option (unlimited force for everyone)</gy>
	6. <r>add graphics quality options</r>
		1. <o>add particles quality options</o>
			1. <o>connect particles quality options</o>
3. <r>add lobby menu</r>
4. <o>add usernames on top of actors</o>
5. <gy>splitscreen multiplayer</gy>
6. <r>controller support</r>
	1. <r>movement & camera</r>
	2. <gy>add menu support with arrow keys & X & O</gy>
7. <gy>powerups to recharge stamina</gy>
8. <gy>steam networking refactor</gy>
9. <r>different maps</r>
10. <r>music</r>
11. <r>refactor & split large overextending scripts eg level, player, etc...</r>

**Bugfixes:**

<gy>unstarted</gy> - <r>documented</r> - <o>unchecked</o> - <g>checked</g>

1. <r>sceneStack doesn't pop on pause menu</r>
1. <o>implement getClosestPointOnCubiod</o>
