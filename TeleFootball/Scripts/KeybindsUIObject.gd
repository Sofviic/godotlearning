extends HBoxContainer

@onready var keybindName = $KeybindName
@onready var button = $Button

var oldButtonValue
var waitingForInput := false

func _on_button_pressed():
	if waitingForInput:
		waitingForInput = false
		button.text = oldButtonValue
		return
	waitingForInput = true
	oldButtonValue = button.text
	button.text = "Press key..."

func _input(event):
	if waitingForInput:
		# TODO: remove code duplication
		if event is InputEventKey:
			waitingForInput = false
			button.text = event.as_text_physical_keycode()
			Keybinds.changeKeybind(keybindName.text, event)
		elif event is InputEventMouseButton:
			waitingForInput = false
			button.text = "Mouse button %d" % event.button_index
			Keybinds.changeKeybind(keybindName.text, event)
	
