extends Control

@onready var forceBar = $MarginContainer/VBoxContainer/Control/ProgressBar
@onready var tutorial = $Tutorial
@onready var tutorialAnimation = $Tutorial/Fade
@onready var hideTutorialDelay = $HideTutorialDelay

func showTutorial():
	tutorial.visible = true
	tutorialAnimation.play("fadeIn")

func hideTutorial():
	hideTutorialDelay.start()

func setForceStamina(val):
	forceBar.value = val

func _on_fade_animation_finished(anim_name):
	if anim_name == "fadeOut":
		tutorial.visible = false

func _on_hide_tutorial_delay_timeout():
	tutorialAnimation.play("fadeOut")
