extends CharacterBody3D

@onready var sprite: AnimatedSprite3D = $Sprite
@onready var forceSound = $ForceSound
@onready var usernameLabel = $Username

const FORCE_VOLUME := -10.0
const FORCE_VOLUME_SILENT := -50.0

var moving := false
var usingForce := false

func playAnimation(animName):
	sprite.play(animName)

func isMoving():
	return moving

func setUsername(username: String):
	if username.length() > 16:
		usernameLabel.text = username.left(13) + "..."
	else:
		usernameLabel.text = username

func _process(delta):
	if usingForce:
		forceSound.volume_db = lerp(forceSound.volume_db, FORCE_VOLUME, delta * 5)
	else:
		forceSound.volume_db = lerp(forceSound.volume_db, FORCE_VOLUME_SILENT, delta * 5)
