extends Node

const PROTOCOL_VERSION = 0

var multiplayerPeer
var peers = {}
var connected := false

signal playerConnected
signal playerDisconnected
signal posRotUpdate
signal ballPosRotUpdate
signal animationUpdate
signal forceUpdate
signal connectionClosed
signal scoresUpdate
signal gameReset

func _ready():
	multiplayer.peer_connected.connect(onPlayerConnected)
	multiplayer.peer_disconnected.connect(onPlayerDisconnected)
	multiplayer.server_disconnected.connect(onServerDisconnected)
	multiplayer.connected_to_server.connect(onServerConected)
	multiplayer.connection_failed.connect(onConnectionFailed)

func hostGame(port):
	multiplayerPeer = ENetMultiplayerPeer.new()
	Global.handleError(multiplayerPeer.create_server(port))
	multiplayer.multiplayer_peer = multiplayerPeer
	connected = true
	print("Hosted game on %d" % port)
	print("Your ID: %d" % multiplayer.get_unique_id())

func joinGame(addr, port):
	multiplayerPeer = ENetMultiplayerPeer.new()
	Global.handleError(multiplayerPeer.create_client(addr, port))
	multiplayer.multiplayer_peer = multiplayerPeer
	print("Joined game on %s:%d" % [addr,port])
	print("Your ID: %d" % multiplayer.get_unique_id())

func closeConnection():
	multiplayer.multiplayer_peer = null
	connected = false
	peers.clear()
	emit_signal("connectionClosed")

func onServerDisconnected():
	closeConnection()

func onServerConected():
	connected = true

func onPlayerDisconnected(id):
	peers.erase(id)
	emit_signal("playerDisconnected", id)

func onConnectionFailed():
	Global.showError("Could not connect to server", false)
	closeConnection()

func onPlayerConnected(id):
	registerPlayer.rpc_id(id, Settings.settings["username"], PROTOCOL_VERSION)

@rpc("any_peer", "unreliable")
func updatePosRot(pos: Vector3, rot: Vector3):
	emit_signal("posRotUpdate", multiplayer.get_remote_sender_id(), pos, rot)

@rpc("authority", "unreliable")
func updateBallPosRot(pos: Vector3, rot: Vector3, vel: Vector3, angVel: Vector3):
	emit_signal("ballPosRotUpdate", multiplayer.get_remote_sender_id(), pos, rot, vel, angVel)

@rpc("any_peer", "unreliable")
func updateAnimation(moving: bool, usingForce: bool):
	emit_signal("animationUpdate", multiplayer.get_remote_sender_id(), moving, usingForce)

@rpc("any_peer", "unreliable")
func applyForce(force: Vector3):
	emit_signal("forceUpdate", multiplayer.get_remote_sender_id(), force)

@rpc("authority", "reliable")
func resetGame():
	emit_signal("gameReset", multiplayer.get_remote_sender_id())

@rpc("any_peer", "reliable")
func registerPlayer(info: String, protocolVersion: int):
	if protocolVersion != PROTOCOL_VERSION:
		Global.showError("Client or server outdated!")
	var new_player_id = multiplayer.get_remote_sender_id()
	peers[new_player_id] = info
	print("Received info from player %s with id %s" % [info, new_player_id])
	emit_signal("playerConnected", new_player_id, info)
	# player_connected.emit(new_player_id, new_player_info)

@rpc("authority", "reliable")
func updateScores(scores: Array[int]):
	emit_signal("scoresUpdate", multiplayer.get_remote_sender_id(), scores)
