extends Node


func map(arr: Array, f: Callable) -> Array:
	var res: Array = []
	for e in arr:
		res.append(f.call(e))
	return res

func dmap(dict: Dictionary, f: Callable) -> Dictionary:
	var res: Dictionary = {}
	for k in dict:
		res[k] = f.call(dict[k])
	return res

func sortOn(arr: Array, f: Callable) -> Array:
	var res: Array = arr.duplicate(false)
	res.sort_custom(f)
	return res
