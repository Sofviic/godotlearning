extends Node


func v3Tov2(vec3: Vector3):
	return Vector2(vec3.x, vec3.z)

func randomUnitCircle(radius: float) -> Vector3:
	var angle: float = randf() * 2 * PI
	return Vector3(cos(angle) * radius, 1, sin(angle) * radius)

