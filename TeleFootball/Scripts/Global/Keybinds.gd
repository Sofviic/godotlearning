extends Node

var defaultKeybinds = {}
var keybinds

func _init():
	defaultKeybinds = getKeybinds()
	keybinds = defaultKeybinds.duplicate(true)

func getKeybinds() -> Dictionary:
	var actions = InputMap.get_actions().filter(func isBuiltIn(k: String): return !k.begins_with("ui_"))
	var kb = {}
	for a in actions:
		kb[a] = InputMap.action_get_events(a).duplicate(true)
	return kb.duplicate(true)

func changeKeybind(keybindName: String, event: InputEvent, save: bool = true):
	if !InputMap.has_action(keybindName):
		Global.showError("Action %s does not exist" % keybindName, false)
		return
	# Erase previous keybindings
	var events = InputMap.action_get_events(keybindName).duplicate(true)
	InputMap.action_erase_events(keybindName)
	# Add new keybindings
	events[0] = event
	for e in events:
		InputMap.action_add_event(keybindName, e)
	
	if save:
		# Update keybinds dictionary
		keybinds = getKeybinds()
		# Save settings
		Settings.settings["keybinds"] = keybinds.duplicate(true)
		Settings.saveSettings()

func applyKeybinds(newKeybinds: Dictionary):
	# Load into keybinds dictionary
	for k in newKeybinds.keys():
		keybinds[k] = []
		for a in newKeybinds[k]:
			keybinds[k].append(a)
		print("Loaded keybind: %s with " % k + str(keybinds[k]))
	
	# Apply keybindings
	for k in keybinds.keys():
		changeKeybind(k, keybinds[k][0], false)

func resetKeybinds():
	Settings.settings["keybinds"] = defaultKeybinds.duplicate()
	applyKeybinds(Settings.settings["keybinds"])
	Settings.saveSettings()
