extends Node

func _input(_event):
	if Input.is_action_just_pressed("fullscreen"):
		match DisplayServer.window_get_mode():
			DisplayServer.WINDOW_MODE_FULLSCREEN: DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
			DisplayServer.WINDOW_MODE_WINDOWED: DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)

func showError(message = "Unknown", quit = true):
	OS.alert("An error occured. Message: %s" % message, "Error")
	if quit:
		get_tree().quit(1)

func handleError(err: Error):
	if err != OK:
		showError(err)
