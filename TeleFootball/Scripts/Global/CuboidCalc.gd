extends Node


func getClosestPointOnCubiod(pos: Vector3, cornerA: Vector3, cornerB: Vector3, radius: float = 0) -> Vector3:
	var cubiod = canoniseCubiod(cornerA, cornerB)
	var region = regionOfPointRelCubiod(cubiod, pos)
	var closest = _getClosestPointOnCanonicalCubiod(pos, cubiod, region)
	var path = closest - pos
	var rpath = path + path.normalized() * radius
	var rclosest = pos + rpath
	return rclosest

func _getClosestPointOnCanonicalCubiod(pos: Vector3, cubiod: Dictionary, region: int) -> Vector3:
	if region in [13]: # 3D cube boundary (1 cube)
		return pos
	if region in [0,2,6,8,18,20,24,26]: # 0D corner boundaries (8 corners)
		var nearestCorner = Functor.sortOn(cubiod.values(), func(c): (c - pos).length())[0]
		return nearestCorner
	if region in [1,3,5,7,9,11,15,17,19,21,23,25]: # 1D edge boundaries (12 edges)
		var nearest2Corners = Functor.sortOn(cubiod.values(), func(c): (c - pos).length()).slice(0, 1)
		return closestPointToLine(pos, nearest2Corners[0], nearest2Corners[1])
	if region in [4,10,12,14,16,22]: # 2D face boundaries (6 faces)
		var nearest4Corners = corners(cubiod, region)
		return closestPointToSurface(pos, nearest4Corners[0], nearest4Corners[1], nearest4Corners[2])
	push_error("<func _getClosestPointOnCanonicalCubiod>: region %d should be in [0..26]!" % region)
	return pos

func canoniseCubiod(cornerA: Vector3, cornerB: Vector3) -> Dictionary:
	return {
		minX = minf(cornerA.x, cornerB.x),
		maxX = maxf(cornerA.x, cornerB.x),
		minY = minf(cornerA.y, cornerB.y),
		maxY = maxf(cornerA.y, cornerB.y),
		minZ = minf(cornerA.z, cornerB.z),
		maxZ = maxf(cornerA.z, cornerB.z),
	}

func regionOfPointRelCubiod(cubiod: Dictionary, pos: Vector3) -> int:
	var x = regionOfPointRelInterval(cubiod.minX, cubiod.maxX, pos.x)
	var y = regionOfPointRelInterval(cubiod.minY, cubiod.maxY, pos.y)
	var z = regionOfPointRelInterval(cubiod.minZ, cubiod.maxZ, pos.z)
	return 9*z+3*y+x
func regionOfPointRelInterval(min: float, max: float, x: float) -> int:
	return (0 if x < min
			else 1 if x < max
			else 2)

func closestPointToLine(x: Vector3, a: Vector3, b: Vector3) -> Vector3:
	var ax = x - a
	var ab = b - a
	return ax - ax.dot(ab) * ab

func closestPointToSurface(x: Vector3, a: Vector3, b: Vector3, c: Vector3) -> Vector3:
	var n = (b - a).cross(c - a).normalized()
	return x - n.dot(x - a) * n

func corners(cubiod: Dictionary, region: int) -> Array:
	match region:
		4: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, true, 0, true, 0, false, cubiod.minZ)
		10: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, true, 0, false, cubiod.minY, true, 0)
		12: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, false, cubiod.minX, true, 0, true, 0)
		14: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, false, cubiod.maxX, true, 0, true, 0)
		16: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, true, 0, false, cubiod.maxY, true, 0)
		22: return varyVec3(cubiod.minX, cubiod.minY, cubiod.minZ, cubiod.maxX, cubiod.maxY, cubiod.maxZ, true, 0, true, 0, false, cubiod.maxZ)
		_: push_error("<func corners>: region %d should be in %s!" % [region, "[4,10,12,14,16,22]"])
	return []

func varyVec3(xa:float,xb:float,ya:float,yb:float,za:float,zb:float,vx:bool=true,dx:float=0,vy:bool=true,dy:float=0,vz:bool=true,dz:float=0,deep:bool=false) -> Array:
	var res = [Vector3(dx,dy,dz)]
	
	if vx:
		var ores = res.duplicate(deep)
		res = []
		for v in ores:
			res.push_back(Vector3(xa,v.y,v.z))
			res.push_back(Vector3(xb,v.y,v.z))
	
	if vy:
		var ores = res.duplicate(deep)
		res = []
		for v in ores:
			res.push_back(Vector3(v.x,ya,v.z))
			res.push_back(Vector3(v.x,yb,v.z))
	
	if vz:
		var ores = res.duplicate(deep)
		res = []
		for v in ores:
			res.push_back(Vector3(v.x,v.y,za))
			res.push_back(Vector3(v.x,v.y,zb))
	
	return res
