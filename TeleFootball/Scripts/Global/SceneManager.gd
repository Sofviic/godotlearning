extends Node

const SCENEDEBUG := false

const game := "res://Scenes/Level.tscn"
const menu := "res://Scenes/Menus/MetaMenu.tscn"
const actor := "res://Scenes/Actor.tscn"
const explosion := "res://Scenes/Particles/Explosion.tscn"

var singleplayer
var paused := false

var sceneStack = [menu]

func changeScene(scene):
	sceneStack.push_back(scene)
	get_tree().change_scene_to_file(scene)
	if SCENEDEBUG:
		print(sceneStack)

func returnToPreviousScene():
	sceneStack.pop_back()
	if sceneStack.is_empty():
		quit()
	get_tree().change_scene_to_file(sceneStack.back())
	if SCENEDEBUG:
		print(sceneStack)

func quit():
	get_tree().quit()
