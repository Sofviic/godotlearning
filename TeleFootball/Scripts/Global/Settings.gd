extends Node

const SETTINGS_FILE_PATH = "user://settings.dat"

@onready var defaultSettings = {
		"username": "Player",
		"addrJoin": "",
		"portJoin": "",
		"portHost": "",
		"hasSeenTutorial": false,
		"fov": 80.0,
		"fovsprint": 12.0,
		"particleQuality": 2,
		"mouseSensitivity": 50,
		"keybinds": Keybinds.defaultKeybinds.duplicate(true)
	}
var settings: Dictionary

func _ready():
	loadSettings()

func resetSettings():
	settings = defaultSettings.duplicate(true)
	saveSettings()


### Loading / saving settings
func loadSettings():
	if !FileAccess.file_exists(SETTINGS_FILE_PATH):
		resetSettings()
		print("Saved default settings in %s" % SETTINGS_FILE_PATH)
	else:
		var file = FileAccess.open(SETTINGS_FILE_PATH, FileAccess.READ)
		settings = file.get_var(true)
		for settingTitle in defaultSettings.keys():
			if !settings.has(settingTitle):
				settings[settingTitle] = defaultSettings[settingTitle].duplicate(true)
				print("Added missing key/value: %s : %s" % [settingTitle, defaultSettings[settingTitle]])
		print("Loaded settings: %s" % settings)
	# Apply keybindings
	Keybinds.applyKeybinds(settings["keybinds"].duplicate(true))

func saveSettings():
	var file = FileAccess.open(SETTINGS_FILE_PATH, FileAccess.WRITE)
	print("Saved settings")
	file.store_var(settings, true)
	print(settings["keybinds"]["right"][0])


### Non-standard getters
func getFOV():
	return settings["fov"]
	
func getSprintFOV():
	return settings["fov"] * (1 + settings["fovsprint"]/100)
	
func getMouseSensitivity():
	return settings["mouseSensitivity"] / 10000.0


