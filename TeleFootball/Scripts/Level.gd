extends Node3D

# REFACTOR THIS
# This has a bunch of multiplayer stuff that shouldn't be the responsibility of the level node;
# Make a multiplayerLevel specific node that is added onto the level in the case of multiplayer.
#
# tl;dr REFACTOR NON LEVEL STUFF OUT OF THE LEVEL SCRIPT

@export var menuScene: PackedScene

@onready var peers = $Peers
@onready var player = $Player
@onready var pauseMenu = $PauseMenu
@onready var ball: RigidBody3D = $Ball
@onready var scoreRed = player.inGameUI.get_node("ScoreInfo/TopPanel/Red/Score")
@onready var scoreBlue = player.inGameUI.get_node("ScoreInfo/TopPanel/Blue/Score")
@onready var timerUI = player.inGameUI.get_node("ScoreInfo/TopPanel/Time/Time")
@onready var cooldownUI = player.inGameUI.get_node("ScoreInfo/Cooldown")
@onready var forceGlow = $Ball/Glow
@onready var forceParticles = $Ball/Trail
@onready var explosionParticles
@onready var ballKickSound = $Ball/KickSound
@onready var room = $Room

@onready var goalResetDelay = $Timers/GoalResetDelay

@onready var jingleGood = $Sounds/JingleGood
@onready var jingleBad = $Sounds/JingleBad

var scores = [0,0]

var peerNodes = {}
var actorScene := preload(SceneManager.actor)
var explosionScene := preload(SceneManager.explosion)

var tutorialVisible := false

var gameTimeSeconds = 0.0

func _ready():
	Multiplayer.connect("playerConnected", addPeer)
	Multiplayer.connect("playerDisconnected", removePeer)
	Multiplayer.connect("posRotUpdate", setPeerPosRot)
	Multiplayer.connect("ballPosRotUpdate", setBallPosRot)
	Multiplayer.connect("animationUpdate", setPeerAnimation)
	Multiplayer.connect("forceUpdate", applyForce)
	Multiplayer.connect("connectionClosed", connectionClosed)
	Multiplayer.connect("scoresUpdate", setScores)
	Multiplayer.connect("gameReset", resetPlayer)
	for id in Multiplayer.peers.keys():
		addPeer(id, Multiplayer.peers[id])
	reset()
	
	print(Settings.settings["hasSeenTutorial"])
	if not Settings.settings["hasSeenTutorial"]:
		player.inGameUI.showTutorial()
		tutorialVisible = true
	
	for g in room.get_node("Goals").get_children():
		g.connect("goal", onGoal)
	
	# Set up particle effects according to settings
	match Settings.settings.particleQuality:
		2:
			forceParticles.visible = true
			forceGlow.visible = false
		0:
			forceParticles.visible = false
			forceGlow.visible = true

func _input(_event):
	if Input.is_action_just_pressed("reset") && (SceneManager.singleplayer || multiplayer.is_server()):
		reset()
	if Input.is_action_just_pressed("menu"):
		toggleMenu()

func _physics_process(_delta):
	if Multiplayer.connected:
		Multiplayer.updatePosRot.rpc(player.position, player.rotation)
		Multiplayer.updateAnimation.rpc(player.moving, player.usingForce)
		if multiplayer.is_server():
			Multiplayer.updateBallPosRot.rpc(ball.position, ball.rotation, ball.linear_velocity, ball.angular_velocity)
	
	if tutorialVisible && player.usingForce:
		tutorialVisible = false
		Settings.settings.hasSeenTutorial = true
		Settings.saveSettings()
		player.inGameUI.hideTutorial()

func _process(delta):
	player.cooldown -= delta
	cooldownUI.text = "%.1fs" % player.cooldown
	cooldownUI.visible = player.cooldown > 0
	var numForceUsers = 0
	
	gameTimeSeconds += delta
	timerUI.text = "%d:%02d" % [floor(gameTimeSeconds / 60), int(floor(gameTimeSeconds)) % 60]
	
	for p in peers.get_children():
		if p.usingForce: numForceUsers += 1
		var towardCamera: Vector2 = VecUtil.v3Tov2(player.position - p.position).normalized()
		var towardsAhead: Vector2 = VecUtil.v3Tov2(-p.get_global_transform().basis.z)
		var towardsRight: Vector2 = VecUtil.v3Tov2(p.get_global_transform().basis.x)
		var facingAngle = towardCamera.dot(towardsAhead)
		var facingAngleRL = towardCamera.dot(towardsRight)
		var facing = "F"
		if facingAngle > 0.25: facing = "F"
		elif facingAngle < -0.25: facing = "B"
		else: facing = "R" if facingAngleRL > 0 else "L"
		var animation = "walking" if p.isMoving() else "idle"
		p.playAnimation(facing + animation)
	
	if player.usingForce: numForceUsers += 1
	
	player.targetShakeStrength = numForceUsers * 0.05
	
	match Settings.settings.particleQuality:
		2: forceParticles.setEmitting(false if numForceUsers == 0 else true)
		0: forceGlow.mesh.material.albedo_color.a = lerp(forceGlow.mesh.material.albedo_color.a, 0.0 if numForceUsers == 0 else 0.75, delta * 3)
		_: Global.showError("Particle quality %d not implemented" % Settings.settings.particleQuality)


func onGoal(team: int):
	scores[team] += 1
	explosionParticles = explosionScene.instantiate()
	add_child(explosionParticles)
	explosionParticles.position = ball.position
	ball.position = Vector3(0,500,0) # Teleport ball away TODO: make this better
	jingleGood.play()
	flushScores()
	if team == 0:
		explosionParticles.emitBlue()
	else:
		explosionParticles.emitRed()
	goalResetDelay.start()

func flushScores():
	updateScoreText()
	if multiplayer.is_server():
		Multiplayer.updateScores.rpc(scores)

func updateScoreText():
	scoreRed.text = str(scores[0])
	scoreBlue.text = str(scores[1])

func reset():
	ball.position = Vector3(0,1,0)
	ball.linear_velocity = Vector3.ZERO
	ball.angular_velocity = Vector3.ZERO
	Multiplayer.resetGame.rpc()
	resetPlayer()

func resetPlayer(_id = 0):
	player.position = VecUtil.randomUnitCircle(5)
	player.velocity = Vector3.ZERO
	player.look_at(Vector3(0,1,0))
	player.cooldown = player.stats.COOLDOWNSECS

func restart():
	scores = [0,0]
	flushScores()
	reset()

func toggleMenu():
	SceneManager.paused = !SceneManager.paused
	pauseMenu.visible = SceneManager.paused
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if SceneManager.paused else Input.MOUSE_MODE_CAPTURED)

func addPeer(id, username):
	var peer = actorScene.instantiate()
	peers.add_child(peer)
	peer.name = str(id)
	peer.setUsername(username)
	peerNodes[id] = peer

func removePeer(id):
	peers.get_node(str(id)).queue_free()

func setPeerPosRot(id, pos, rot):
	var node = peers.get_node(str(id))
	if node != null:
		node.position = pos
		node.rotation = rot
	else: push_warning("Peer %s does not exist as node" % id)

func setPeerAnimation(id, moving, usingForce):
	var node = peers.get_node(str(id))
	if node != null:
		node.moving = moving
		node.usingForce = usingForce
	else: push_warning("Peer %s does not exist as node" % id)

func setBallPosRot(_id, pos, rot, vel, angVel):
	ball.position = pos
	ball.rotation = rot
	ball.linear_velocity = vel
	ball.angular_velocity = angVel
	
func setScores(_id, newScores):
	scores = newScores
	updateScoreText()

func connectionClosed():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	SceneManager.changeScene(SceneManager.menu)

func applyForce(_id, force):
	ball.apply_central_force(force)

func _on_ball_body_entered(body):
	if body.is_in_group("BounceSound"):
		ballKickSound.pitch_scale = randf_range(0.9, 1.1)
		ballKickSound.play()

func _on_goal_reset_delay_timeout():
	explosionParticles.queue_free()
	if GameSettings.settings.resetOnGoal: reset()

