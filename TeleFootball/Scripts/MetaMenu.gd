extends Control

@onready var menus = {
	"main": $MainMenu,
	"multiplayer": $MultiplayerMenu,
	"options": $OptionsMenu,
	"keybinds": $KeybindsMenu
}

var menuStack = ["main"]

func goto(menuName: String):
	for k in menus.keys():
		menus[k].visible = k == menuName
	menuStack.push_back(menuName)
	print("Goto called. Stack: " + str(menuStack))

func goBack():
	menuStack.pop_back()
	if menuStack.is_empty():
		Global.showError("Menu stack empty")
	var targetMenu = menuStack.back()
	for k in menus.keys():
		menus[k].visible = k == targetMenu
	print("Go back called. Stack: " + str(menuStack))
