extends Control

@onready var fov = $CenterContainer/HBoxContainer/FOV/HSlider
@onready var fovsprint = $CenterContainer/HBoxContainer/FOVSprint/HSlider
@onready var particleQuality = $CenterContainer/HBoxContainer/Particles/OptionButton
@onready var mouseSensitivity = $CenterContainer/HBoxContainer/MouseSensitivity/HSlider
@onready var menuManager = get_parent()

func _ready():
	flush()

func flush():
	fov.value = Settings.settings.fov
	fovsprint.value = Settings.settings.fovsprint
	particleQuality.selected = Settings.settings.particleQuality
	mouseSensitivity.value = Settings.settings.mouseSensitivity

func _on_reset_all_settings_button_up():
	Settings.reset_settings()
	flush()

func _on_fov_value_changed(value: float):
	Settings.settings.fov = value
	fov.tooltip_text = "%.0f°" % Settings.settings.fov
	fovsprint.tooltip_text = "%.0f°" % (Settings.settings.fov * (1 + Settings.settings.fovsprint/100))

func _on_fovsprint_value_changed(value: float):
	Settings.settings.fovsprint = value
	fovsprint.tooltip_text = "%.0f%% (%.0f°)" % [Settings.settings.fovsprint,(Settings.settings.fov * (1 + Settings.settings.fovsprint/100))]

func _on_option_button_item_selected(index):
	Settings.settings.particleQuality = index

func _on_mouse_sensitivity_value_changed(value):
	Settings.settings.mouseSensitivity = value
	mouseSensitivity.tooltip_text = "%d" % value


func _on_reset_tutorial_pressed():
	Settings.settings.hasSeenTutorial = false
	Settings.saveSettings()


func _on_keybinds_pressed():
	menuManager.goto("keybinds")


func _on_back_pressed():
	Settings.saveSettings()
	menuManager.goBack()
