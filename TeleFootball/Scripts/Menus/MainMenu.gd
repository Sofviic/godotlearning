extends Control

@onready var menuManager = get_parent()

func _on_singleplayer_pressed():
	SceneManager.singleplayer = true
	SceneManager.changeScene(SceneManager.game)

func _on_multiplayer_pressed():
	menuManager.goto("multiplayer")

func _on_options_pressed():
	menuManager.goto("options")

func _on_exit_pressed():
	SceneManager.quit()
