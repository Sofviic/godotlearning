extends Control

var keybindsUIObject = preload("res://Scenes/UIObjects/KeybindsUIObject.tscn")

@onready var keybindsContainer = $CenterContainer/HBoxContainer/ScrollContainer/VBoxContainer
@onready var menuManager = get_parent()

func _ready():
	for k in Keybinds.keybinds.keys():
		var eventKey = Keybinds.keybinds[k][0]
		
		# TODO: Also add mouse buttons
		if eventKey is InputEventKey:
			var inst = keybindsUIObject.instantiate()
			keybindsContainer.add_child(inst)
			inst.keybindName.text = k
			inst.button.text = eventKey.as_text_physical_keycode()
		elif eventKey is InputEventMouseButton:
			var inst = keybindsUIObject.instantiate()
			keybindsContainer.add_child(inst)
			inst.keybindName.text = k
			inst.button.text = "Mouse button %d" % eventKey.button_index
		else:
			print("Error: Event key not yet implemented: %s" % eventKey)


func _on_back_pressed():
	menuManager.goBack()


func _on_default_keybinds_pressed():
	Keybinds.resetKeybinds()
	get_tree().reload_current_scene()
