extends Control

@onready var username = $CenterContainer/HBoxContainer/Username/Username
@onready var addrJoin = $CenterContainer/HBoxContainer/Inputs/Joining/Address/AddrJoin
@onready var portJoin = $CenterContainer/HBoxContainer/Inputs/Joining/Port/PortJoin
#@onready var addrHost = $CenterContainer/HBoxContainer/Inputs/Hosting/Address/AddrHost
@onready var portHost = $CenterContainer/HBoxContainer/Inputs/Hosting/Port/PortHost
@onready var menuManager = get_parent()

func _ready():
	username.text = Settings.settings["username"]
	addrJoin.text = Settings.settings["addrJoin"]
	portJoin.text = Settings.settings["portJoin"]
	portHost.text = Settings.settings["portHost"]

func _on_username_text_changed(new_text):
	Settings.settings["username"] = new_text
	Settings.saveSettings()

func _on_addr_join_text_changed(new_text):
	Settings.settings["addrJoin"] = new_text
	Settings.saveSettings()


func _on_port_join_text_changed(new_text):
	Settings.settings["portJoin"] = new_text
	Settings.saveSettings()


func _on_port_host_text_changed(new_text):
	Settings.settings["portHost"] = new_text
	Settings.saveSettings()


func _on_join_pressed():
	var addr = addrJoin.placeholder_text if addrJoin.text.is_empty() else addrJoin.text
	var port = portJoin.placeholder_text if portJoin.text.is_empty() else portJoin.text
	print("Joining game on address %s:%s ..." % [addr,port])
	Multiplayer.joinGame(addr, int(port))
	SceneManager.singleplayer = false
	SceneManager.changeScene(SceneManager.game)


func _on_host_pressed():
	var port = portHost.placeholder_text if portHost.text.is_empty() else portHost.text
	print("Hosting game on port %s ..." % port)
	Multiplayer.hostGame(int(port))
	SceneManager.singleplayer = false
	SceneManager.changeScene(SceneManager.game)


func _on_back_pressed():
	menuManager.goBack()
