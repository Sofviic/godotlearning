extends Control

@onready var resetButton = $CenterContainer/HBoxContainer/Reset
@onready var restartButton = $CenterContainer/HBoxContainer/Restart
@onready var level = get_parent()

func _ready():
	resetButton.disabled = !SceneManager.singleplayer && !multiplayer.is_server()
	restartButton.disabled = !SceneManager.singleplayer && !multiplayer.is_server()

func _on_continue_pressed():
	level.toggleMenu()

func _on_reset_pressed():
	if SceneManager.singleplayer || multiplayer.is_server():
		level.reset()

func _on_restart_pressed():
	if SceneManager.singleplayer || multiplayer.is_server():
		level.restart()

func _on_options_pressed():
	Global.showError("options should be shown here :P")

func _on_main_menu_pressed():
	Multiplayer.closeConnection()
	SceneManager.changeScene(SceneManager.menu)
