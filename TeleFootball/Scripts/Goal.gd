extends Node3D

signal goal

@export var team: int

func _on_area_3d_body_entered(body):
	if body.is_in_group("Ball"):
		emit_signal("goal", team)
	else:
		print("Goal: AAAAAAAAAAAAAAAHHHHH Someone that's not a ball entered me!!! {0}".format([body]))
