extends CharacterBody3D

@onready var stats = $PlayerStats
@onready var forceController = $ForceController

@export var acceleration : float = 10.0
@export var deacceleration : float = 20.0

@onready var camera: Camera3D = $Camera
@onready var ball: RigidBody3D = get_node("../Ball")
@onready var inGameUI = $InGameUI

@onready var staminaRegainSpeed = stats.DEFAULT_STAMINA_REGAIN_SPEED
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@onready var cameraDefaultPos = $Camera.position
@onready var forceSound = $ForceSound
@onready var staminaCooldown = $StaminaCooldown
var cooldown = 1.0
var moving := false
var usingForce := false
var shakeStrength := 0.0
var targetShakeStrength := 0.0 # Is set by Level.gd script
var forceStamina := 1.0

var noise = FastNoiseLite.new()

func _ready():
	noise.fractal_octaves = 1
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if !SceneManager.paused:
		#var look_dir = Input.get_vector("look_left", "look_right", "look_up", "look_down")
		if event is InputEventMouseMotion:
			rotate_y(-event.relative.x * Settings.getMouseSensitivity())
			camera.rotate_x(-event.relative.y * Settings.getMouseSensitivity())
		elif event is InputEventJoypadMotion: # TODO: Buffer ie make it into a toggle; rn it only rotates on value change
			if event.axis == JOY_AXIS_RIGHT_X:
				rotate_y(-event.axis_value / 10)
			if event.axis == JOY_AXIS_RIGHT_Y:
				camera.rotate_x(-event.axis_value / 10)
		camera.rotation.x = clamp(camera.rotation.x, -PI/2, PI/2)
	if Input.is_action_just_pressed("hideHud"):
		inGameUI.visible = !inGameUI.visible

var time = 0.0
func _process(delta):
	time += delta * stats.SHAKE_SPEED
	var shakeX = noise.get_noise_2d(time,0) * shakeStrength
	var shakeY = noise.get_noise_2d(0,time) * shakeStrength
	camera.position = cameraDefaultPos + Vector3(shakeX, shakeY, 0)
	
	var staminaChange = stats.STAMINA_LOSE_SPEED if usingForce else staminaRegainSpeed
	forceStamina = clamp(forceStamina + delta * staminaChange, 0, 1)
	if forceStamina == 0 && staminaCooldown.is_stopped():
		staminaRegainSpeed = 0
		staminaCooldown.start()
	
	inGameUI.setForceStamina(forceStamina)
	
	shakeStrength = lerp(shakeStrength, targetShakeStrength if usingForce else 0.0, delta * 2)
	forceSound.volume_db = lerp(forceSound.volume_db, stats.FORCE_VOLUME if usingForce else stats.FORCE_VOLUME_SILENT, delta * 5)

func _physics_process(delta):
	usingForce = false
	if !SceneManager.paused && cooldown <= 0 && forceStamina > 0:
		forceController.handleForce(ball, position)
		usingForce = Input.is_action_pressed("forcePush") or Input.is_action_pressed("forcePull")
	
	if not is_on_floor():
		velocity.y -= gravity * delta
	elif !SceneManager.paused && Input.is_action_just_pressed("jump"):
		velocity.y = stats.JUMP_VELOCITY

	var input_dir = Input.get_vector("left", "right", "forwards", "backwards")
	moving = input_dir.length_squared() > 0
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var speed = stats.SPRINT if Input.is_action_pressed("sprint") else stats.SPEED
	
	if cooldown <= 0:
		if direction:
			# Acceleration
			velocity.x = lerp(velocity.x, direction.x * speed, acceleration * delta)
			velocity.z = lerp(velocity.z, direction.z * speed, acceleration * delta)
		else:
			# Deacceleration
			velocity.x = lerp(velocity.x, 0.0, deacceleration * delta)
			velocity.z = lerp(velocity.z, 0.0, deacceleration * delta)
	
	var fov = Settings.getSprintFOV() if Input.is_action_pressed("sprint") && direction.length_squared() != 0 else Settings.getFOV()
	camera.fov = lerp(camera.fov, fov, delta * 10)

	move_and_slide()


func _on_stamina_cooldown_timeout():
	staminaRegainSpeed = stats.DEFAULT_STAMINA_REGAIN_SPEED
