extends Node

func handleForce(ball, position):
	if Input.is_action_pressed("forcePush"):
		var force_vector = (ball.position - position).normalized() * 15 + Vector3(0,10,0)
		ball.apply_central_force(force_vector)
		if Multiplayer.connected:
			Multiplayer.applyForce.rpc(force_vector)
	if Input.is_action_pressed("forcePull"):
		var force_vector = (ball.position - position).normalized() * -15  + Vector3(0,10,0)
		ball.apply_central_force(force_vector)
		if Multiplayer.connected:
			Multiplayer.applyForce.rpc(force_vector)

