extends Node

const SPEED = 5.0
const SPRINT = 10.0
const JUMP_VELOCITY = 4.5
const SHAKE_SPEED = 600 # Move to Settings
const FORCE_VOLUME := -5.0 # Move to Settings
const FORCE_VOLUME_SILENT := -50.0 # Move to Settings
const COOLDOWNSECS = 2.0
const DEFAULT_STAMINA_REGAIN_SPEED := 0.05
const STAMINA_LOSE_SPEED := -0.5
