extends Node3D

const ROTATION_SPEED = -0.1

@onready var rotationCenter = $Football/RotationCenter
@onready var lightsRotationCenter = $Football/LightRotationCenter
@onready var room = $Room

func _ready():
	for c in room.get_node("Lights").get_children():
		c.light_energy *= 0.3

func _process(delta):
	rotationCenter.rotation.y += delta * ROTATION_SPEED
	lightsRotationCenter.rotation.y -= delta * ROTATION_SPEED * 5
